# Approval Testing Workshop

Hi, my name is Kaspar.

## Table of Contents
- [Target group](#markdown-header-target-group)
- [Topics covered](#markdown-header-topics-covered)
- [Requirements](#markdown-header-requirements)
- [Project descriptipn](#markdown-header-project-description)
- [Exercises](#markdown-header-exercises)

## Target group
This workshop is intended for developers/testers with zero or very few prior experience with Approval testing.
Previous Java programming experience is highly recommended.
The participant should be familiar with using an IDE such as IntelliJ or Eclipse, and Git.

## Topics covered
1. Features of [Approval Tests Java](https://github.com/approvals/ApprovalTests.Java) library
2. Replace assertions with approvals
3. Testing complex objects
4. Locking down logic in untested legacy code

## Requirements
- Preferred IDE
- Java11
- Git

## Project description
The project consist of 2 different coding katas:
- [Supermarket Receipt Refactoring Kata](https://github.com/emilybache/SupermarketReceipt-Refactoring-Kata)
- [Product Export Refactoring Kata](https://github.com/emilybache/Product-Export-Refactoring-Kata)

## Exercises

### Exercise 1:
The supermarket has a catalogue of different products. Each product has a price, name and unit. Every once in a while there can be special offers for some products. Currently, there are defined following special offers:

- Buy three for the price of two
- Percent discount
- Buy two for an amount
- Buy five for an amount

For every purchase, you get a receipt with the total price and any discounts that were applied.

You have been given a task to add a new feature to the system. Unfortunately, the system has none or very few automated tests. You can assume that the system is working correctly, but before adding new features, make sure that the old functionality remains the same and does not break. Fortunately, you have recently learned about Approval testing and have a chance to make use of it.

#### Task
* Your task is to replace any existing tests with Approvals tests. After that, make sure that all the different discounts are applied correctly.

### Exercise 2:
There is a store that consists of different products. Each product has a name, id, weight and a price. An order consists of products and the store it was made to. The system has the functionality to export each order as XML, however the code is messy and contains duplications. It was decided that the export functionality needs to be refactored and you have been given a task to lock down existing logic and verify that it is working properly. Currently there is the possibility to:

- Export multiple orders
- Export tax details of given orders
- Export store information
- Export history of given orders

#### Task
* Your task is to test every method of XMLExporter
* Figure out how to test date dependant methods
