package workshop.exercise1.model;

public enum ProductUnit {
    Kilo, Each
}
