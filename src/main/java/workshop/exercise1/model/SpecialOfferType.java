package workshop.exercise1.model;

public enum SpecialOfferType {
    ThreeForTwo, PercentDiscount, TwoForAmount, FiveForAmount;
}
