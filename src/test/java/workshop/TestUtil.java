package workshop;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import static com.fasterxml.jackson.databind.SerializationFeature.ORDER_MAP_ENTRIES_BY_KEYS;

public class TestUtil {

    private static final ObjectMapper objectMapper = new ObjectMapper();

    public static String prettyPrintJson(Object object) {
        try {
            return objectMapper.writerWithDefaultPrettyPrinter().withFeatures(ORDER_MAP_ENTRIES_BY_KEYS).writeValueAsString(object);
        } catch (JsonProcessingException e) {
            return e.getMessage();
        }
    }
}
