package workshop.exercise2;

import org.approvaltests.Approvals;
import org.junit.jupiter.api.Test;

import java.util.List;

public class XMLExporterTest {

    @Test
    void name() {
        Approvals.verifyXml(XMLExporter.exportFull(List.of(SampleModelObjects.RecentOrder)));
    }
}
